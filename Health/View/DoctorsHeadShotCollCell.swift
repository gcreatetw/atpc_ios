//
//  DoctorsHeadShotCollCell.swift
//  Health
//
//  Created by Yang Nina on 2021/6/19.
//

import UIKit

class DoctorsHeadShotCollCell: UICollectionViewCell {

    @IBOutlet weak var doctorsImg: UIImageView!
    @IBOutlet weak var doctorsView: UIImageView!
    @IBOutlet weak var doctorsName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        doctorsView.layer.cornerRadius = doctorsView.bounds.width/2
        doctorsImg.layer.cornerRadius = doctorsImg.bounds.width/2
        // Initialization code
    }
}
