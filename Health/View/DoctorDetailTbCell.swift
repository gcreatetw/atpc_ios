//
//  DoctorDetailTbCell.swift
//  Health
//
//  Created by Yang Nina on 2021/6/24.
//

import UIKit

class DoctorDetailTbCell: UITableViewCell {

    @IBOutlet weak var dactorNameLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var specializedLabel: UILabel!
    @IBOutlet weak var certificateLabel: UILabel!
    @IBOutlet weak var expertiseLabel: UILabel!
    @IBOutlet weak var experienceLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
   
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
