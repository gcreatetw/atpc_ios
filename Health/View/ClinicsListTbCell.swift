//
//  ClinicsListTbCell.swift
//  Health
//
//  Created by Yang Nina on 2021/6/16.
//

import UIKit

class ClinicsListTbCell: UITableViewCell {

    @IBOutlet weak var clinicListImageView: UIImageView!
    @IBOutlet weak var clinicListName: UILabel!
    @IBOutlet weak var clinicListAddress: UILabel!
    @IBOutlet weak var clinicListPhone: UILabel!
    @IBOutlet weak var noImg: UIImageView!
    @IBOutlet weak var distance: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clinicListImageView.layer.cornerRadius = clinicListImageView.bounds.width / 2

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
