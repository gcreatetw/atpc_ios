//
//  ClinicsBannerCollectionViewCell.swift
//  Health
//
//  Created by Yang Nina on 2021/6/16.
//

import UIKit

class ClinicsBannerCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var clinicBannerImageView: UIImageView!
    
    @IBOutlet weak var knowledgeBannerImageView: UIImageView!
}
