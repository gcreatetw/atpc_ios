//
//  KnowledgeTableViewCell.swift
//  Health
//
//  Created by Yang Nina on 2021/6/17.
//

import UIKit

class KnowledgeTableViewCell: UITableViewCell {

    @IBOutlet weak var articleImg: UIImageView!
    @IBOutlet weak var articleDateLabel: UILabel!
    @IBOutlet weak var articleTitleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
