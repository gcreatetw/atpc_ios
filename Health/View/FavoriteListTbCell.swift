//
//  FavoriteListTbCell.swift
//  Health
//
//  Created by Yang Nina on 2021/6/24.
//

import UIKit

class FavoriteListTbCell: UITableViewCell {

    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var favoriteImg: UIImageView!
    @IBOutlet weak var favoriteNameLabel: UILabel!
    @IBOutlet weak var favoriteAddressLabel: UILabel!
    @IBOutlet weak var favoritePhoneLabel: UILabel!
    @IBOutlet weak var noImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        favoriteImg.layer.cornerRadius = favoriteImg.bounds.width/2
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
