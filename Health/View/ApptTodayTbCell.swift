//
//  ApptTodayTbCell.swift
//  Health
//
//  Created by Yang Nina on 2021/6/22.
//

import UIKit

class ApptTodayTbCell: UITableViewCell {

    @IBOutlet weak var roomNameLabel: UILabel!
    @IBOutlet weak var curNumLabel: UILabel!
    @IBOutlet weak var roomUpdateTimeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
