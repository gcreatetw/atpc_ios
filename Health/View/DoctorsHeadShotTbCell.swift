//
//  DoctorsHeadShotTbCell.swift
//  Health
//
//  Created by Yang Nina on 2021/6/19.
//

import UIKit
protocol CollectionViewCellDelegate: class {
    func collectionView(collectionviewcell: DoctorsCollCell?, index: Int, didTappedInTableViewCell: DoctorsHeadShotTbCell)
    // other delegate methods that you can define to perform action in viewcontroller
}

class DoctorsHeadShotTbCell: UITableViewCell {
    weak var cellDelegate: CollectionViewCellDelegate?
    var doctorList = [Clinic_doctors]()
    @IBOutlet weak var doctorsCollectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        configureCellSize()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureCellSize(){
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset.right = 40
        layout.minimumLineSpacing = 20
        layout.scrollDirection = .horizontal
        layout.itemSize = CGSize(width: 56, height: 75)
        doctorsCollectionView.collectionViewLayout = layout
        doctorsCollectionView.backgroundColor = UIColor.white
        doctorsCollectionView.delegate = self
        doctorsCollectionView.dataSource = self
    }
    //加載
    func reloadData(doctors:[Clinic_doctors]) {
        doctorList = doctors
        self.doctorsCollectionView.reloadData()
    }
}
extension DoctorsHeadShotTbCell: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return doctorList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "\(DoctorsCollCell.self)", for: indexPath) as? DoctorsCollCell else { return UICollectionViewCell()}
        cell.doctorsName.text = doctorList[indexPath.item].clinic_doctor_name
        let url = doctorList[indexPath.item].clinic_doctor_picture_url!
        cell.doctorsImg.image = UIImage(named: "illus_no-doctor")
        cell.bgView.backgroundColor = UIColor.clear
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let data = data{
                DispatchQueue.main.async {
                    cell.bgView.backgroundColor = UIColor(named: "main_green")
                    cell.doctorsImg.image = UIImage(data: data)
                }
            }
        }.resume()
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? DoctorsCollCell
        print("I'm tapping the \(indexPath.item)")
        self.cellDelegate?.collectionView(collectionviewcell: cell, index: indexPath.item, didTappedInTableViewCell: self)
    }
}

