//
//  DoctorsCollCell.swift
//  Health
//
//  Created by Yang Nina on 2021/6/29.
//

import UIKit

class DoctorsCollCell: UICollectionViewCell {
    @IBOutlet weak var doctorsImg: UIImageView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var doctorsName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.layer.cornerRadius = bgView.bounds.width/2
        doctorsImg.layer.cornerRadius = doctorsImg.bounds.width/2
        // Initialization code
    }
}
