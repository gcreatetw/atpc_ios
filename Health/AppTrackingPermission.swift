//
//  AppTrackingPermission.swift
//  Health
//
//  Created by Yi Chun Chiu on 2023/8/22.
//

import AppTrackingTransparency

enum AppTrackingPermissionError: Error {
  case denied
  case notDetermined
  case restricted
}

class AppTrackingPermission {
  
  func requestAppTrackingPermission(completion: @escaping (Bool, AppTrackingPermissionError?) -> ()) {
    ATTrackingManager.requestTrackingAuthorization { trackingAuthorizationStatus in
      switch trackingAuthorizationStatus {
        case .authorized:
          print(trackingAuthorizationStatus)
          completion(true, nil)
        case .denied:
          print(trackingAuthorizationStatus)
          completion(false, .denied)
        case .notDetermined:
          print(trackingAuthorizationStatus)
          completion(false, .notDetermined)
        case .restricted:
          print(trackingAuthorizationStatus)
          completion(false, .restricted)
        @unknown default:
          break
      }
    }
  }
}
