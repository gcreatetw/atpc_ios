//
//  DataStruct.swift
//  Health
//
//  Created by Yang Nina on 2021/6/21.
//

import Foundation

struct ClinicsLists: Decodable {
    let home_page_banner:[URL]
    let clinics_info:[Clinics_info]
}
struct Clinics_info: Decodable {
    var clinic_id:Int
    let clinic_name:String
    let clinic_img_url:String?
    let clinic_tel:String
    let clinic_adress:String
}
//診所資訊
struct ClinicsDetail:Decodable {
    let right_time_clinic_id:String?
    //let clinic_id:Int
    let clinic_img_url:String?
    let clinic_name:String?
    let clinic_description:String?
    let clinic_tel:String?
    let clinic_address:String?
    let clinic_website:String?
    let clinic_fb:String?
    let clinic_line:String?
    //let clinic_month_img_url:URL?//班表
    let business_hours:Business_hours
    let duty_doctors:[Duty_doctors]
    let clinic_doctors:[Clinic_doctors]
    let room: [Room]
}
struct Business_hours:Decodable {
    let morning:String?
    let afternoon:String?
    let evening:String?
}
struct Duty_doctors:Decodable {
    let morning_doctor:String?
    let afternoon_doctor:String?
    let evening_doctor:String?
    let day:String?
}
struct Clinic_doctors:Decodable{
    let clinic_doctor_id:Int
    let clinic_doctor_name:String?
    let clinic_doctor_picture_url:URL?
}
//醫師資訊
struct DoctorsDetail:Decodable {
    let doctor_picture_url:URL
    let doctor_social_media:String?//FB
    let doctor_blog:String?
    let doctor_name:String
    let doctor_gender:String
    let doctor_specialist_number:String?
    let doctor_forensic_medicine_number:String
    let doctor_licenses:String
    let doctor_expertises:String
    let doctor_educations:String
    let doctor_clinics:[Doctor_clinics]
}
struct Doctor_clinics:Decodable {
    let clinic_id:String?
    let clinic_name:String?
}
//文章
struct Article:Decodable {
    var posts_page_banner = [URL]()
    let posts:[Posts]
}
struct Posts: Decodable {
    let post_title:String
    let post_img_url:String?
    let post_date:String
    let post_url:URL
}

//診間號碼
struct ClinicNum: Decodable {
    let clinic_id:Int
    let name:String
    let address:String
    let contact:String
    let room:[Room]
}
struct Room: Decodable{
    let room_id:Int
    let name:String
    let currentNum:Int
}
