//
//  DateUtil.swift
//  Health
//
//  Created by Yang Nina on 2021/6/22.
//

import Foundation
class DateUtil: NSObject {
    static let shared = DateUtil()
    func getStrSysDate(withFormat strFormat: String) -> String {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = strFormat
        dateFormatter.locale = Locale.ReferenceType.system
        dateFormatter.timeZone = TimeZone.ReferenceType.system
        return dateFormatter.string(from: Date())
    }

    func getStrSysWeekDay() -> String {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "eeee"
        dateFormatter.locale = Locale.ReferenceType.system
        dateFormatter.timeZone = TimeZone.ReferenceType.system
        let weekDay = dateFormatter.string(from: Date())
        if weekDay == "Mon" {
            return "monday"
        } else if weekDay == "Tue" {
            return "tuesday"
        } else if weekDay == "Wed" {
            return "wednesday"
        } else if weekDay == "Thu" {
            return "thursday"
        } else if weekDay == "Fri" {
            return "friday"
        } else if weekDay == "Sat" {
            return "saturday"
        } else if weekDay == "Sun" {
            return "sunday"
        } else {
            return weekDay
        }
    }
}
