//
//  DoctorDetailViewController.swift
//  Health
//
//  Created by Yang Nina on 2021/6/22.
//

import UIKit

class DoctorDetailViewController: UIViewController {
    var doctorsDetail:DoctorsDetail?
    
    @IBOutlet weak var doctorIntroScrollView: UIScrollView!
    @IBOutlet weak var doctorImg: UIImageView!
    @IBOutlet weak var doctorNameLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var specializedLabel: UILabel!
    @IBOutlet weak var certificateLabel: UILabel!
    @IBOutlet weak var expertiseLabel: UILabel!
    @IBOutlet weak var experienceLabel: UILabel!
    func fetchDoctorDetail(id:Int){
        let urlStr = "http://www.imh.tw/wp-json/get_doctor_info_api/v1?id=\(id)"
        if let url = URL(string: urlStr){
            print(urlStr)
            URLSession.shared.dataTask(with: url) { data, response, error in
                //guard let self = self else { return }
                if let data = data {
                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .iso8601
                    do {
                        let searchResponse = try decoder.decode(DoctorsDetail.self, from: data)
                        DispatchQueue.main.async {
                            URLSession.shared.dataTask(with: searchResponse.doctor_picture_url) { data, response, error in
                                if let dataImg = data{
                                    DispatchQueue.main.async {
                                        self.doctorImg.image = UIImage(data: dataImg)
                                    }
                                }
                            }.resume()
                            self.doctorsDetail = searchResponse
                            self.doctorNameLabel.text = searchResponse.doctor_name + "醫師"
                            self.genderLabel.text = searchResponse.doctor_gender
                            self.specializedLabel.text = searchResponse.doctor_specialist_number! + "\n" + searchResponse.doctor_forensic_medicine_number
                            let tmpcertificate = searchResponse.doctor_licenses
                            var certificate:String = ""
                            for i in tmpcertificate{
                                if i == "," || i == "，"{
                                    certificate.append("\n")
                                }
                                else{
                                    certificate.append(i)
                                }
                            }
                            self.certificateLabel.text = certificate
                            self.expertiseLabel.text = "‧" + searchResponse
                                .doctor_expertises
                            let tmpexperience = searchResponse.doctor_educations
                            var experience:String = "‧"
                            for i in tmpexperience{
                                if i == "," || i == "，"{
                                    experience.append("\n" + "‧")
                                }
                                else{
                                    experience.append(i)
                                }
                            }
                            self.experienceLabel.text = experience
                        }
                    } catch {
                        print(error)
                    }

                }
            }.resume()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        doctorImg.layer.cornerRadius = doctorImg.bounds.width/2
    }

    @IBAction func goToLink(_ sender: UIButton) {
        if sender.tag == 0{
            if let url = URL(string: (doctorsDetail?.doctor_social_media)!) {
                UIApplication.shared.open(url)
            }
        }
        else if sender.tag == 1{
            if let url = URL(string: (doctorsDetail?.doctor_blog)!) {
                UIApplication.shared.open(url)
            }
        }
    }
}
extension DoctorDetailViewController: UIScrollViewDelegate{

}
