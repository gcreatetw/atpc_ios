//
//  ClinicDetailViewController.swift
//  Health
//
//  Created by Yang Nina on 2021/6/17.
//

import UIKit
import CoreData
class ClinicDetailViewController: UIViewController {
    var clinicsDetail:ClinicsDetail?
    var dutyDoctors = [Duty_doctors]()
    var clinicDoctor = [Clinic_doctors]()
    var clinicNum:ClinicNum?
    var clinicRoom = [Room]()
    var weekTabValue = String()
    var readFavoriteInfos = [Favorite]()
    var refreshControl: UIRefreshControl!

    @IBOutlet weak var introView: UIView!
    @IBOutlet weak var clinicDetailImg: UIImageView!
    @IBOutlet weak var clinicTel: UITextView!
    @IBOutlet weak var clinicAddress: UITextView!
    @IBOutlet weak var clinicWeb: UITextView!
    @IBOutlet weak var clinicIntroTableView: UITableView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var morningLabel: UILabel!
    @IBOutlet weak var morningDuty: UILabel!
    @IBOutlet weak var afternoonLabel: UILabel!
    @IBOutlet weak var afternoonDuty: UILabel!
    @IBOutlet weak var eveningLabel: UILabel!
    @IBOutlet weak var eveningDuty: UILabel!
    let addFavoriteBtn = UIButton()
    func fetchClinicDetail(id:Int) {
        let urlStr = "http://www.imh.tw/wp-json/get_clinic_info_api/v1?id=\(id)"
        if let url = URL(string: urlStr){
            URLSession.shared.dataTask(with: url) {[weak self] data, response, error in
                guard let self = self else { return }
                if let data = data {
                    let decoder = JSONDecoder()
                    do {
                        let searchResponse = try decoder.decode(ClinicsDetail.self, from: data)
                        DispatchQueue.main.async {
                            self.clinicsDetail = searchResponse
                            if let urlImg = URL(string: searchResponse.clinic_img_url!){
                                URLSession.shared.dataTask(with: urlImg) { data2, response2, error2 in
                                    DispatchQueue.main.async {
                                        self.clinicDetailImg.image = UIImage(data: data2!)
                                    }
                                }.resume()
                            }
                            self.clinicTel.text = searchResponse.clinic_tel
                            self.clinicAddress.text = searchResponse.clinic_address
                            self.clinicWeb.text = searchResponse.clinic_website
                            self.morningLabel.text = searchResponse.business_hours.morning
                            self.afternoonLabel.text = searchResponse.business_hours.afternoon
                            self.eveningLabel.text = searchResponse.business_hours.evening
                            for i in 0...6{
                                if self.segmentedControl.selectedSegmentIndex == i{
                                    let tmpMorning:String = searchResponse.duty_doctors[i].morning_doctor!
                                    var Morning:String = ""
                                    for i in tmpMorning {
                                        if i == "," || i == "，"{
                                            Morning.append("\n")
                                        }
                                        else{
                                            Morning.append(i)
                                        }
                                    }
                                    self.morningDuty.text = Morning
                                    
                                    let tmpAfternoon:String = searchResponse.duty_doctors[i].afternoon_doctor!
                                    var Afternoon:String = ""
                                    for i in tmpAfternoon {
                                        if i == "," || i == "，"{
                                            Afternoon.append("\n")
                                        }
                                        else{
                                            Afternoon.append(i)
                                        }
                                    }
                                    self.afternoonDuty.text = Afternoon
                                    
                                    let tmpEvening:String = searchResponse.duty_doctors[i].evening_doctor!
                                    var Evening:String = ""
                                    for i in tmpEvening {
                                        if i == "," || i == "，"{
                                            Evening.append("\n")
                                        }
                                        else{
                                            Evening.append(i)
                                        }
                                    }
                                    self.eveningDuty.text = Evening
                                }
                            }
                            self.fetchClinicNum(Num: searchResponse.right_time_clinic_id)
                            self.dutyDoctors = (self.clinicsDetail?.duty_doctors)!
                            self.clinicDoctor = (self.clinicsDetail?.clinic_doctors)!
                            self.navigationItem.backButtonTitle = ""
                            self.initUINavRightButton()
                            self.navigationController?.navigationBar.backgroundColor = UIColor.white
                            self.clinicIntroTableView.reloadData()
                        }
                    } catch {
                        print(error)
                    }

                }
            }.resume()
        }
    }
    func fetchClinicNum(Num:String?){
        let urlStr = "https://app.right-time.com.tw:443/api/clinic/\(Num ?? "")/info"
        if let url = URL(string: urlStr){
            URLSession.shared.dataTask(with: url) { data, response, error in
                if let data = data{
                    let decoder = JSONDecoder()
                    do {
                        let searchResponse = try decoder.decode(ClinicNum.self, from: data)
                        print(searchResponse)
                        DispatchQueue.main.async {
                            self.clinicNum = searchResponse
                            self.clinicRoom = (self.clinicNum?.room)!
                            self.clinicIntroTableView.reloadData()
                        }
                    }
                    catch {
                        print(error)
                    }
                }
            }.resume()
        }
        
    }
    var thisId:Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        clinicIntroTableView.backgroundColor = UIColor.white
        initBtn()
        let attributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        let attributesSelected = [ NSAttributedString.Key.foregroundColor : UIColor(named: "main_green")]
        segmentedControl.setTitleTextAttributes(attributes, for: .normal)
        segmentedControl.setTitleTextAttributes(attributesSelected as [NSAttributedString.Key : Any], for: .selected)
        fetchClinicDetail(id: thisId)
        self.weekTabValueInti()
        segmentedControl.addTarget(self, action: #selector(ClinicDetailViewController.segmentedValueChanged(_:)), for: .valueChanged)
        refreshControl = UIRefreshControl()
        clinicIntroTableView.addSubview(refreshControl)
        self.refreshControl.addTarget(self, action: #selector(ClinicDetailViewController.refreshClinicDetailPage), for: UIControl.Event.valueChanged)
        
    }
    @objc func refreshClinicDetailPage() {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
            self.fetchClinicNum(Num: self.clinicsDetail?.right_time_clinic_id)
            self.refreshControl.endRefreshing()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        initBtn()
    }
    @objc func initBtn(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        do {
            readFavoriteInfos = try context.fetch(Favorite.fetchRequest())
        } catch {
            print("error")
        }
        //判斷是否有存到收藏清單內
        var tmpReadFavoriteInfos:[Int] = []
        for i in self.readFavoriteInfos {
            tmpReadFavoriteInfos.append(Int(i.clinic_id))
        }
        if tmpReadFavoriteInfos.contains(thisId) {
            self.addFavoriteBtn.isSelected = true
        } else {
            self.addFavoriteBtn.isSelected = false
        }
    }
    func initUINavRightButton() {
        addFavoriteBtn.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
        addFavoriteBtn.setImage(UIImage(named: "icon_like_default"), for: UIControl.State.normal)
        addFavoriteBtn.setImage(UIImage(named: "icon_like_selected"), for: UIControl.State.selected)
        addFavoriteBtn.addTarget(self, action: #selector(ClinicDetailViewController.favoriteBtnPressed(_:)), for: .touchUpInside)
        
        let favoriteBtn = UIBarButtonItem(customView: addFavoriteBtn)
        
       self.navigationItem.rightBarButtonItem = favoriteBtn
    }
    @objc func favoriteBtnPressed(_ sender: UIButton) {
        if sender.isSelected == true {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            do {
                readFavoriteInfos = try context.fetch(Favorite.fetchRequest())
            } catch {
                print("error")
            }
            
            for i in readFavoriteInfos {
                if i.clinic_id == thisId {
                    context.delete(i)
                }
            }
            sender.isSelected = false
        } else {
            
            saveData(id:thisId , name: clinicsDetail?.clinic_name ?? "", address: clinicsDetail?.clinic_address ?? "", tel: clinicsDetail?.clinic_tel ?? "", img: NSURL(string: clinicsDetail!.clinic_img_url!)! as URL)
            sender.isSelected = true
        }
    }
    func saveData(id: Int, name: String, address: String, tel: String, img:URL) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let directory = Favorite(context: appDelegate.persistentContainer.viewContext)
        directory.clinic_id = Int16(id)
        directory.clinic_name = name
        directory.clinic_address = address
        directory.clinic_tel = tel
        directory.clinic_img = img
        appDelegate.saveContext()
    }
    func weekTabValueInti() {
        weekTabValue = DateUtil.shared.getStrSysWeekDay()
        if weekTabValue == "monday" {
            segmentedControl.selectedSegmentIndex = 0
        } else if weekTabValue == "tuesday" {
            segmentedControl.selectedSegmentIndex = 1
        } else if weekTabValue == "wednesday" {
            segmentedControl.selectedSegmentIndex = 2
        } else if weekTabValue == "thursday" {
            segmentedControl.selectedSegmentIndex = 3
        } else if weekTabValue == "friday" {
            segmentedControl.selectedSegmentIndex = 4
        } else if weekTabValue == "saturday" {
            segmentedControl.selectedSegmentIndex = 5
        } else if weekTabValue == "sunday" {
            segmentedControl.selectedSegmentIndex = 6
        } else {
            segmentedControl.selectedSegmentIndex = 7
        }
    }
    @objc func segmentedValueChanged(_ sender: UISegmentedControl!) {
        switch sender.selectedSegmentIndex {
        case 0:
            weekTabValue = "monday"
            let tmpMorning:String = dutyDoctors[0].morning_doctor!
            var Morning:String = ""
            for i in tmpMorning {
                if i == "," || i == "，"{
                    Morning.append("\n")
                }
                else{
                    Morning.append(i)
                }
            }
            morningDuty.text = Morning
            let tmpAfternoon:String = dutyDoctors[0].afternoon_doctor!
            var Afternoon:String = ""
            for i in tmpAfternoon {
                if i == "," || i == "，"{
                    Afternoon.append("\n")
                }
                else{
                    Afternoon.append(i)
                }
            }
            afternoonDuty.text = Afternoon
            
            let tmpEvening:String = dutyDoctors[0].evening_doctor!
            var Evening:String = ""
            for i in tmpEvening {
                if i == "," || i == "，"{
                    Evening.append("\n")
                }
                else{
                    Evening.append(i)
                }
            }
            eveningDuty.text = Evening
            self.clinicIntroTableView.reloadData()
        case 1:
            weekTabValue = "tuesday"
            let tmpMorning:String = dutyDoctors[1].morning_doctor!
            var Morning:String = ""
            for i in tmpMorning {
                if i == "," || i == "，"{
                    Morning.append("\n")
                }
                else{
                    Morning.append(i)
                }
            }
            morningDuty.text = Morning
            let tmpAfternoon:String = dutyDoctors[1].afternoon_doctor!
            var Afternoon:String = ""
            for i in tmpAfternoon {
                if i == "," || i == "，"{
                    Afternoon.append("\n")
                }
                else{
                    Afternoon.append(i)
                }
            }
            afternoonDuty.text = Afternoon
            
            let tmpEvening:String = dutyDoctors[1].evening_doctor!
            var Evening:String = ""
            for i in tmpEvening {
                if i == "," || i == "，"{
                    Evening.append("\n")
                }
                else{
                    Evening.append(i)
                }
            }
            eveningDuty.text = Evening
            self.clinicIntroTableView.reloadData()
        case 2:
            weekTabValue = "wednesday"
            let tmpMorning:String = dutyDoctors[2].morning_doctor!
            var Morning:String = ""
            for i in tmpMorning {
                if i == "," || i == "，"{
                    Morning.append("\n")
                }
                else{
                    Morning.append(i)
                }
            }
            morningDuty.text = Morning
            let tmpAfternoon:String = dutyDoctors[2].afternoon_doctor!
            var Afternoon:String = ""
            for i in tmpAfternoon {
                if i == "," || i == "，"{
                    Afternoon.append("\n")
                }
                else{
                    Afternoon.append(i)
                }
            }
            afternoonDuty.text = Afternoon
            
            let tmpEvening:String = dutyDoctors[2].evening_doctor!
            var Evening:String = ""
            for i in tmpEvening {
                if i == "," || i == "，"{
                    Evening.append("\n")
                }
                else{
                    Evening.append(i)
                }
            }
            eveningDuty.text = Evening
            self.clinicIntroTableView.reloadData()
        case 3:
            weekTabValue = "thursday"
            let tmpMorning:String = dutyDoctors[3].morning_doctor!
            var Morning:String = ""
            for i in tmpMorning {
                if i == "," || i == "，"{
                    Morning.append("\n")
                }
                else{
                    Morning.append(i)
                }
            }
            morningDuty.text = Morning
            let tmpAfternoon:String = dutyDoctors[3].afternoon_doctor!
            var Afternoon:String = ""
            for i in tmpAfternoon {
                if i == "," || i == "，"{
                    Afternoon.append("\n")
                }
                else{
                    Afternoon.append(i)
                }
            }
            afternoonDuty.text = Afternoon
            
            let tmpEvening:String = dutyDoctors[3].evening_doctor!
            var Evening:String = ""
            for i in tmpEvening {
                if i == "," || i == "，"{
                    Evening.append("\n")
                }
                else{
                    Evening.append(i)
                }
            }
            eveningDuty.text = Evening
            self.clinicIntroTableView.reloadData()
        case 4:
            weekTabValue = "friday"
            let tmpMorning:String = dutyDoctors[4].morning_doctor!
            var Morning:String = ""
            for i in tmpMorning {
                if i == "," || i == "，"{
                    Morning.append("\n")
                }
                else{
                    Morning.append(i)
                }
            }
            morningDuty.text = Morning
            let tmpAfternoon:String = dutyDoctors[4].afternoon_doctor!
            var Afternoon:String = ""
            for i in tmpAfternoon {
                if i == "," || i == "，"{
                    Afternoon.append("\n")
                }
                else{
                    Afternoon.append(i)
                }
            }
            afternoonDuty.text = Afternoon
            
            let tmpEvening:String = dutyDoctors[4].evening_doctor!
            var Evening:String = ""
            for i in tmpEvening {
                if i == "," || i == "，"{
                    Evening.append("\n")
                }
                else{
                    Evening.append(i)
                }
            }
            eveningDuty.text = Evening
            self.clinicIntroTableView.reloadData()
        case 5:
            weekTabValue = "saturday"
            let tmpMorning:String = dutyDoctors[5].morning_doctor!
            var Morning:String = ""
            for i in tmpMorning {
                if i == "," || i == "，"{
                    Morning.append("\n")
                }
                else{
                    Morning.append(i)
                }
            }
            morningDuty.text = Morning
            let tmpAfternoon:String = dutyDoctors[5].afternoon_doctor!
            var Afternoon:String = ""
            for i in tmpAfternoon {
                if i == "," || i == "，"{
                    Afternoon.append("\n")
                }
                else{
                    Afternoon.append(i)
                }
            }
            afternoonDuty.text = Afternoon
            
            let tmpEvening:String = dutyDoctors[5].evening_doctor!
            var Evening:String = ""
            for i in tmpEvening {
                if i == "," || i == "，"{
                    Evening.append("\n")
                }
                else{
                    Evening.append(i)
                }
            }
            eveningDuty.text = Evening
            
            self.clinicIntroTableView.reloadData()
        case 6:
            weekTabValue = "sunday"
            let tmpMorning:String = dutyDoctors[6].morning_doctor!
            var Morning:String = ""
            for i in tmpMorning {
                if i == "," || i == "，"{
                    Morning.append("\n")
                }
                else{
                    Morning.append(i)
                }
            }
            morningDuty.text = Morning
            let tmpAfternoon:String = dutyDoctors[6].afternoon_doctor!
            var Afternoon:String = ""
            for i in tmpAfternoon {
                if i == "," || i == "，"{
                    Afternoon.append("\n")
                }
                else{
                    Afternoon.append(i)
                }
            }
            afternoonDuty.text = Afternoon
            
            let tmpEvening:String = dutyDoctors[6].evening_doctor!
            var Evening:String = ""
            for i in tmpEvening {
                if i == "," || i == "，"{
                    Evening.append("\n")
                }
                else{
                    Evening.append(i)
                }
            }
            eveningDuty.text = Evening
            self.clinicIntroTableView.reloadData()
        default:
            weekTabValue = "monday"
            let tmpMorning:String = dutyDoctors[0].morning_doctor!
            var Morning:String = ""
            for i in tmpMorning {
                if i == "," || i == "，"{
                    Morning.append("\n")
                }
                else{
                    Morning.append(i)
                }
            }
            morningDuty.text = Morning
            let tmpAfternoon:String = dutyDoctors[0].afternoon_doctor!
            var Afternoon:String = ""
            for i in tmpAfternoon {
                if i == "," || i == "，"{
                    Afternoon.append("\n")
                }
                else{
                    Afternoon.append(i)
                }
            }
            afternoonDuty.text = Afternoon
            
            let tmpEvening:String = dutyDoctors[0].evening_doctor!
            var Evening:String = ""
            for i in tmpEvening {
                if i == "," || i == "，"{
                    Evening.append("\n")
                }
                else{
                    Evening.append(i)
                }
            }
            eveningDuty.text = Evening
            self.clinicIntroTableView.reloadData()
        }
    }
    @IBAction func goToLink(_ sender: UIButton) {
        if sender.tag == 0{
            if let url = URL(string: (clinicsDetail?.clinic_fb)!){
                UIApplication.shared.open(url)
            }
        }
        else if sender.tag == 1{
            if let url = URL(string: (clinicsDetail?.clinic_line)!) {
                UIApplication.shared.open(url)
            }
        }
    }
}
extension ClinicDetailViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }
        else{
            //return 3
            return clinicRoom.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.backgroundColor = .white
        if indexPath.section == 0{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "\(DoctorsHeadShotTbCell.self)", for: indexPath) as? DoctorsHeadShotTbCell else { return UITableViewCell() }
            cell.cellDelegate = self
            if indexPath.item < clinicDoctor.count {
                cell.reloadData(doctors: clinicDoctor)
            }
        return cell
        }
        else if indexPath.section == 1{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "\(ApptTodayTbCell.self)", for: indexPath) as? ApptTodayTbCell else { return UITableViewCell() }
            cell.roomNameLabel.text = clinicRoom[indexPath.row].name
            cell.curNumLabel.text = "\(clinicRoom[indexPath.row].currentNum)"
            cell.roomUpdateTimeLabel.text = DateUtil.shared.getStrSysDate(withFormat: "yyyy/MM/dd HH:mm")
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 100
        }
        else if indexPath.section == 1 {
            return 120
        }
        return CGFloat()
    }
}
extension ClinicDetailViewController: CollectionViewCellDelegate {
    func collectionView(collectionviewcell: DoctorsCollCell?, index: Int, didTappedInTableViewCell: DoctorsHeadShotTbCell) {
        if let controller = storyboard?.instantiateViewController(withIdentifier: "\(DoctorDetailViewController.self)") as? DoctorDetailViewController {
            let item = didTappedInTableViewCell.doctorList
            let doctorcell = item[index].clinic_doctor_id
            controller.fetchDoctorDetail(id: Int(doctorcell))
            navigationController?.pushViewController(controller, animated: true)
            }
    }
}
