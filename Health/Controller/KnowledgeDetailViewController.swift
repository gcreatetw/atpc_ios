//
//  KnowledgeDetailViewController.swift
//  Health
//
//  Created by Yang Nina on 2021/6/25.
//

import UIKit
import WebKit

class KnowledgeDetailViewController: UIViewController {

    let postUrl:URL
    @IBOutlet weak var webView: WKWebView!
    init?(coder:NSCoder,postUrl:URL){
        self.postUrl = postUrl
        super.init(coder: coder)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let request = URLRequest(url: postUrl)
        webView.load(request)
        
        // Do any additional setup after loading the view.
    }
    

}
