//
//  ClinicPickerViewController.swift
//  Health
//
//  Created by Yang Nina on 2021/6/18.
//

import UIKit

class ClinicPickerViewController: UIViewController {

    @IBOutlet weak var clinicListPicker: UIPickerView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.clinicListPicker.tintColor = UIColor.black
        selectedCityRow()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func goBackToClinicListPage(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    var cityName: Citys?
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        cityName = Citys(rawValue: city)
    }
}
var city = String()
extension ClinicPickerViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    //顯示幾列
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    //各列有多少行資料
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Citys.allCases.count
    }
    //每個選項顯示的資料
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        return Citys.allCases[row].rawValue
//    }
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        return NSAttributedString(string: Citys.allCases[row].rawValue, attributes: [NSAttributedString.Key.foregroundColor:UIColor(named: "main_black") as Any])
    }
    //選擇後執行的動作
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        city = Citys.allCases[row].rawValue
    }
    func selectedCityRow() {
        let userDefault = UserDefaults.standard
        let name = userDefault.string(forKey: "city_id")
        for i in 0 ..< Citys.allCases.count {
            if Citys.allCases[i].rawValue == name{
                self.clinicListPicker.selectRow(i, inComponent: 0, animated: true)
            }
        }
        self.clinicListPicker.reloadAllComponents()
        return
    }
}

