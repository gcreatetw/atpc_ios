//
//  KnowledgeViewController.swift
//  Health
//
//  Created by Yang Nina on 2021/6/17.
//

import UIKit

class KnowledgeViewController: UIViewController {
    var articles:Article?
    var post = [Posts]()
    var bannerImg:[URL] = []
    var imgIndex = 0
    var newpost = [Posts]()
    var isLoading = false
    var loadPage:Int = 1
    @IBOutlet weak var knowledgeTableView: UITableView!
    @IBOutlet weak var knowledgeBannerCollectionView: UICollectionView!
    @IBOutlet weak var knowledgePageControl: UIPageControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        knowledgeBannerCollectionView.delegate = self
        knowledgeBannerCollectionView.dataSource = self
        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(changeBanner), userInfo: nil, repeats: true)
        knowledgeTableView.backgroundColor = UIColor.white
        fetchArticle()
    }
    func fetchArticle(){
         let strUrl = "http://www.imh.tw/wp-json/get_posts/v1?page=1"
          if let url = URL(string: strUrl){
               URLSession.shared.dataTask(with: url) { data, response, error in
                if let data = data{
                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .secondsSince1970
                    do {
                        let searchResponse = try decoder.decode(Article.self, from: data)
                        DispatchQueue.main.async {
                            self.isLoading = false
                            self.articles = searchResponse
                            self.post = (self.articles?.posts)!
                            self.bannerImg = (self.articles?.posts_page_banner)!
                            self.navigationItem.backButtonTitle = ""
                            self.navigationController?.navigationBar.backgroundColor = UIColor.white
                            self.knowledgeBannerCollectionView.reloadData()
                            self.knowledgeTableView.reloadData()
                        }
                    }
                    catch {
                        print(error)
                    }
                }
            }.resume()
            
        }
    }

    @IBSegueAction func showArticle(_ coder: NSCoder) -> KnowledgeDetailViewController? {
        guard let row = knowledgeTableView.indexPathForSelectedRow?.row else { return nil }
        return KnowledgeDetailViewController(coder: coder, postUrl: post[row].post_url)
    }
    func fetchMore(page:Int){
        let strUrl = "http://www.imh.tw/wp-json/get_posts/v1?page=\(page)"
        if let url = URL(string: strUrl){
            URLSession.shared.dataTask(with: url) { data, response, error in
                if let data = data{
                    let decoder = JSONDecoder()
                    do {
                        let searchResponse = try decoder.decode(Article.self, from: data)
                        DispatchQueue.main.async {
                            self.articles = searchResponse
                            self.newpost = (self.articles?.posts)!
                        }
                    }
                    catch {
                        print(error)
                    }
                }
            }.resume()
        }
    }
    func loadMoreData(){
        if !self.isLoading{
            self.isLoading = true
            DispatchQueue.global().async {
                sleep(2)
                self.loadPage += 1
                self.fetchMore(page: self.loadPage)
                DispatchQueue.main.async {
                    if self.newpost.count == 0 {
                        self.isLoading = false
                        self.loadPage -= 1
                    }
                    else{
                        var newIndexPaths = [IndexPath]()
                        for rowPosition in 0..<self.newpost.count {
                            let newIndexPath = IndexPath(row: self.post.count + rowPosition, section: 0)
                            newIndexPaths.append(newIndexPath)
                        }
                        self.post += self.newpost
                        self.knowledgeTableView.insertRows(at: newIndexPaths, with: .automatic)
                        //self.knowledgeTableView.reloadData()
                        self.isLoading = false
                    }
                }
            }
        }
        
    }
}
extension KnowledgeViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return post.count
        }
        else if section == 1 {
            return 1
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.backgroundColor = .white
        if indexPath.section == 0{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "\(KnowledgeTableViewCell.self)", for: indexPath) as? KnowledgeTableViewCell else { return UITableViewCell() }
            let articleRow = post[indexPath.row]
            let imgurl = URL(string: articleRow.post_img_url!) ?? URL(fileURLWithPath: "")
            cell.articleImg.image = nil
            URLSession.shared.dataTask(with: imgurl) { data, response, error in
                if let data = data{
                    DispatchQueue.main.async {
                        cell.articleImg.image = UIImage(data: data)
                    }
                }
            }.resume()
            cell.articleTitleLabel.text = articleRow.post_title
            cell.articleDateLabel.text = articleRow.post_date
            return cell
        }
        else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "\(LoadingTableViewCell.self)", for: indexPath) as? LoadingTableViewCell else { return UITableViewCell() }
            cell.activityIndicator.startAnimating()
            if isLoading == true{
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
                    cell.isHidden = true
                }
            }
            else{
                cell.isHidden = false
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 231
        }
        else{
            return 55
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        if (offsetY > contentHeight - scrollView.frame.height * 4) && !isLoading{
            loadMoreData()
        }
    }
}
extension KnowledgeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    @objc func changeBanner(){
        imgIndex += 1
        var indexPath:IndexPath
        if imgIndex == bannerImg.count{
            imgIndex = 0
        }
        indexPath = IndexPath(item: imgIndex, section: 0)
        knowledgeBannerCollectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return bannerImg.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "\(ClinicsBannerCollectionViewCell.self)", for: indexPath) as? ClinicsBannerCollectionViewCell else { return UICollectionViewCell() }
        cell.knowledgeBannerImageView.image = nil
        URLSession.shared.dataTask(with: bannerImg[indexPath.item]) { data, response, error in
            if let data = data{
                DispatchQueue.main.async {
                    cell.knowledgeBannerImageView.image = UIImage(data: data)
                    }
                }
            }.resume()
        knowledgePageControl.numberOfPages = bannerImg.count
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let page = scrollView.contentOffset.x/scrollView.bounds.width
        knowledgePageControl.currentPage = Int(page)
    }
}
