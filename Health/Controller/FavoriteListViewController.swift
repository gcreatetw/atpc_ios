//
//  FavoriteListViewController.swift
//  Health
//
//  Created by Yang Nina on 2021/6/24.
//

import UIKit
import CoreData

class FavoriteListViewController: UIViewController {

    @IBOutlet weak var favoriteTableView: UITableView!
    var favoriteList = [Favorite]()
    var appDelegate = UIApplication.shared.delegate as? AppDelegate
    override func viewDidLoad() {
        super.viewDidLoad()
        favoriteTableView.backgroundColor = UIColor.white
        self.navigationItem.backButtonTitle = ""
        self.navigationController?.navigationBar.backgroundColor = UIColor.white
        fetchCoreData()
        // Do any additional setup after loading the view.
    }
    func fetchCoreData(){
        if let context = appDelegate?.persistentContainer.viewContext{
            do {
                favoriteList = try context.fetch(Favorite.fetchRequest())
            } catch {
                print("error")
            }
            if favoriteList.count == 0 {
                favoriteTableView.isHidden = true
            } else {
                favoriteTableView.isHidden = false
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        fetchCoreData()
        favoriteTableView.reloadData()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension FavoriteListViewController:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favoriteList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "\(FavoriteListTbCell.self)", for: indexPath) as? FavoriteListTbCell else { return UITableViewCell() }
        cell.noImg.isHidden = false
        cell.favoriteImg.backgroundColor = UIColor(named: "light_gray")
        let favoriteItem = favoriteList[indexPath.row]
        cell.favoriteNameLabel.text = favoriteItem.clinic_name
        cell.favoriteAddressLabel.text = favoriteItem.clinic_address
        cell.favoritePhoneLabel.text = favoriteItem.clinic_tel
        URLSession.shared.dataTask(with: favoriteItem.clinic_img!) { data, response, error in
            if let data = data {
                DispatchQueue.main.async {
                    cell.noImg.isHidden = true
                    cell.favoriteImg.image = UIImage(data: data)
                }
            }
        }.resume()
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let controller = storyboard?.instantiateViewController(withIdentifier: "\(ClinicDetailViewController.self)") as? ClinicDetailViewController {
            let favorite = favoriteList[indexPath.row].clinic_id
            controller.thisId = Int(favorite)
            //controller.fetchClinicDetail(id: Int(favorite))
            navigationController?.pushViewController(controller, animated: true)
        }
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delAction = UIContextualAction(style: .normal, title: "") { (action, view, completionHandler) in
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
           
            for i in self.favoriteList {
                if i.clinic_name == self.favoriteList[indexPath.row].clinic_name {
                    context.delete(i)
                    appDelegate.saveContext()
                }
            }
            completionHandler(true)
            self.fetchCoreData()
            tableView.reloadData()
        }
        delAction.backgroundColor = UIColor(named: "num_red")
        delAction.image = UIImage(named: "delete")
        return UISwipeActionsConfiguration(actions: [delAction])
    }
    
}
