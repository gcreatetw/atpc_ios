//
//  ClinicListViewController.swift
//  Health
//
//  Created by Yang Nina on 2021/6/16.
//

import UIKit
import CoreLocation

class ClinicListViewController: UIViewController {
    var clinicsLists:ClinicsLists?
    var clinics = [Clinics_info]()
    var bannerImage:[URL] = []
    @IBOutlet weak var clinicBannerCollectionView: UICollectionView!
    @IBOutlet weak var clinicBannerPageControl: UIPageControl!
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var hospitalsTableView: UITableView!
    @IBOutlet weak var noClinicView: UIView!
    var imageIndex = 0
    let locationManager = CLLocationManager()
    var timer: Timer?

    //var locations = [Location]()
    
    @IBAction func unwindToClinicList(_ unwindSegue: UIStoryboardSegue) {
        if let sourceViewController = unwindSegue.source as? ClinicPickerViewController{
            cityName.text = sourceViewController.cityName?.rawValue
            fetchItems()
        }
    }
    
    func fetchItems() {
        //轉百分比是optional(可能會失敗)
        if let strUrl = "http://www.imh.tw/wp-json/get_clinics_api/v1?county=\(cityName.text ?? "台北市")".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
           let url = URL(string: strUrl){
            URLSession.shared.dataTask(with: url) { data, response, error in
                if let data = data{
                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .iso8601
                    do {
                        //.self變實際可以存的東西
                        let searchResponse = try decoder.decode(ClinicsLists.self, from: data)
                        print(searchResponse)
                        DispatchQueue.main.async {
                            self.clinicsLists = searchResponse
                            self.clinics = searchResponse.clinics_info
                            self.bannerImage = searchResponse.home_page_banner
                            if self.clinics.count == 0{
                                self.hospitalsTableView.isHidden = true
                            }
                            else{
                                self.hospitalsTableView.isHidden = false
                            }
                            self.clinicBannerCollectionView.reloadData()
                            self.hospitalsTableView.reloadData()
                            self.addBannerScrollTimer()
                        }
                    }
                    catch {
                        print(error)
                    }
                }
            }.resume()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        clinicBannerCollectionView.delegate = self
        clinicBannerCollectionView.dataSource = self
        hospitalsTableView.backgroundColor = .white
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        navigationItem.backButtonTitle = ""
        navigationController?.navigationBar.backgroundColor = UIColor.white
        fetchItems()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer?.invalidate()
    }
    
    private func addBannerScrollTimer() {
        timer?.invalidate()
        timer = nil
        _ = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.changeBanner), userInfo: nil, repeats: true)
    }
    @objc func changeBanner(){
        imageIndex += 1
        var indexPath:IndexPath
        if imageIndex == bannerImage.count{
            imageIndex = 0
        }
        indexPath = IndexPath(item: imageIndex, section: 0)
        clinicBannerCollectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally)
    }
    
    @IBAction func goToClinicPicker(_ sender: Any) {
        let userDefault = UserDefaults.standard
        userDefault.set(cityName.text, forKey: "city_id")
    }
}
extension ClinicListViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return clinics.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "\(ClinicsListTbCell.self)", for: indexPath) as? ClinicsListTbCell else { return UITableViewCell() }
        let tmpImg = cell.clinicListImageView.image
        cell.clinicListImageView.image = nil
        cell.noImg.isHidden = false
        cell.clinicListImageView.backgroundColor = UIColor(named: "light_gray")
        let item = clinics[indexPath.row]
        if let imgUrl = URL(string:item.clinic_img_url!){
            URLSession.shared.dataTask(with: imgUrl) { data, response, error in
                if let data = data{
                    DispatchQueue.main.async {
                        if tmpImg == cell.clinicListImageView.image{
                            cell.noImg.isHidden = true
                            cell.clinicListImageView.image = UIImage(data: data)
                        }
                    }
                }
            }.resume()
        }
        cell.clinicListName.text = item.clinic_name
        cell.clinicListAddress.text = item.clinic_adress
        cell.clinicListPhone.text = item.clinic_tel
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let controller = storyboard?.instantiateViewController(withIdentifier: "\(ClinicDetailViewController.self)") as? ClinicDetailViewController {
            let clinicId = clinicsLists?.clinics_info[indexPath.row].clinic_id
            controller.thisId = clinicId!
            //controller.fetchClinicDetail(id: clinicId ?? 0)
            navigationController?.pushViewController(controller, animated: true)
        }
    }
}

extension ClinicListViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return bannerImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "\(ClinicsBannerCollectionViewCell.self)", for: indexPath) as? ClinicsBannerCollectionViewCell else { return UICollectionViewCell() }
        cell.clinicBannerImageView.image = nil
        URLSession.shared.dataTask(with: bannerImage[indexPath.item]) { data, response, error in
            if let data = data{
                DispatchQueue.main.async {
                    cell.clinicBannerImageView.contentMode = .scaleAspectFit
                    cell.clinicBannerImageView.image = UIImage(data: data)
                }
            }
        }.resume()
        clinicBannerPageControl.numberOfPages = bannerImage.count
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let page = scrollView.contentOffset.x/scrollView.bounds.width
        clinicBannerPageControl.currentPage = Int(page)
    }
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        let page = scrollView.contentOffset.x/scrollView.bounds.width
        clinicBannerPageControl.currentPage = Int(page)
    }
    
}

extension ClinicListViewController: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let getLocation = locations[0] as CLLocation
        let loc: CLLocation = CLLocation(latitude: getLocation.coordinate.latitude, longitude: getLocation.coordinate.longitude)
        let locale = Locale(identifier: "zh_TW")
        CLGeocoder().reverseGeocodeLocation(loc, preferredLocale: locale) { [self] placemarks, error in
            if error == nil {
                let pm = placemarks! as [CLPlacemark]
                if pm.count > 0 {
                    let pm = placemarks![0]
                    cityName.text = pm.subAdministrativeArea
                    print("pm.subAdministrativeArea==========\(pm.subAdministrativeArea)")
                }
            }
        }
        locationManager.stopUpdatingLocation()
    }
}
